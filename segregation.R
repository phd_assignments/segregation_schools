  library(segregation)
  library(tidyr)
  library(data.table)
  library(dplyr)
  library(writexl)
  library(ggplot2)
  
  # Preparar el set de datos en el formato requerido para el paquete de segregacion
  
  to_pivot <- r_a %>% select(id, provincia, sector, ambito, anho, BoliviaPrimaria, ParaguayPrimaria, 
                             PerúPrimaria, VenezuelaPrimaria, OtrosMigrantes, nativos)
  
  
  seg_migrantes <- colnames(to_pivot[, 6:11])
  
  seg_migrantes <- pivot_longer(to_pivot, cols = all_of(seg_migrantes),
                       names_to = "Origen", values_to = "Cantidad")
  
  rm(to_pivot)
  
  
  # Cuantificamos los origenes por año
  
  vol_mig <- seg_migrantes %>% group_by(anho, Origen) %>% 
    summarise(Cantidades =sum(Cantidad, na.rm=TRUE))
  vol_mig
  
  vol_mig <- pivot_wider(vol_mig, names_from = Origen, values_from = Cantidades)
  vol_mig
  
  # write_xlsx(vol_mig, "volumen_migrantes.xlsx")
  
  # Cantidad de migrantes por jurisdicción
  mig_by_province <- seg_migrantes %>% filter(Origen != "nativos") %>% 
    group_by(provincia, anho) %>% 
    summarise(Cantidades = sum(Cantidad, na.rm = TRUE))
  mig_by_province <- pivot_wider(mig_by_province, names_from = anho, values_from = Cantidades)
  mig_by_province <- mig_by_province %>% filter(!is.na(provincia))
  mig_by_province <- mig_by_province %>%
    mutate(Total = rowSums(across(colnames(mig_by_province[,2:13])), na.rm=TRUE)) %>% 
    arrange(desc(Total))
  
  # Volumen de migrantes BsAs
  
  vol_mig <- seg_migrantes %>% filter(provincia == "Buenos Aires") %>% 
    group_by(anho, Origen) %>% 
    summarise(Cantidades = sum(Cantidad, na.rm=TRUE))
  
  vol_mig <- pivot_wider(vol_mig, names_from = Origen, values_from = Cantidades)
  vol_mig
  
# volumen de migrantes por sector

vol_sector <- seg_migrantes %>% filter(sector == "Estatal") %>% 
  group_by(anho, Origen) %>% 
  summarise(Cantidades = sum(Cantidad, na.rm=TRUE))
vol_sector <- pivot_wider(vol_sector, names_from = Origen, values_from = Cantidades)
  
# write_xlsx(vol_sector, "sector_migrantes.xlsx")

# volumen de migrantes por sector

vol_ambito <- seg_migrantes %>% filter(ambito == "Urbano") %>% 
  group_by(anho, Origen) %>% 
  summarise(Cantidades = sum(Cantidad, na.rm=TRUE))
vol_ambito <- pivot_wider(vol_ambito, names_from = Origen, values_from = Cantidades)

# write_xlsx(vol_ambito, "ambito_migrantes.xlsx")
  
  # Volumen de migrantes CABA
  
  vol_mig <- seg_migrantes %>% filter(provincia == "Ciudad de Buenos Aires") %>% 
    group_by(anho, Origen) %>% 
    summarise(Cantidades = sum(Cantidad, na.rm=TRUE))
  
  vol_mig <- pivot_wider(vol_mig, names_from = Origen, values_from = Cantidades)
  vol_mig
  
  # write_xlsx(vol_mig, "CABA_migrantes.xlsx")
  
  # Volumen de migrantes CABA
  
  vol_mig <- seg_migrantes %>% filter(provincia == "Cordoba") %>% 
    group_by(anho, Origen) %>% 
    summarise(Cantidades = sum(Cantidad, na.rm=TRUE))
  
  vol_mig <- pivot_wider(vol_mig, names_from = Origen, values_from = Cantidades)
  vol_mig

# write_xlsx(vol_mig, "cordoba_migrantes.xlsx")
  
  
  # Calculo de la segracion total de los migrantes
  # Crear un dataframe donde guardar
  segregations <- data.frame()
  for (year in 2011:2022) {
    # Filtrar por año y calcular la segregación total
    seg_year <- seg_migrantes %>% filter(anho == year) %>% 
      mutual_total("Origen", "id", weight = "Cantidad")
    # Asignar el año correspondiente
    seg_year$year <- as.factor(year)
    seg_year$decomposition <- "Total"
    # Agregar el resultado al dataframe creado anteriormente
    segregations <- bind_rows(segregations, seg_year)
    
  }
  segregations <- pivot_wider(segregations, names_from = stat, values_from = est)
  
  
  # Descomposición del indice: "Segregacion entre provincias"
  between_province <- data.frame()
  for (year in 2011:2022) {
    # Filtrar por año y calcular la segregación total
    seg_year <- seg_migrantes %>% filter(anho == year) %>% 
      mutual_total("Origen", "provincia", weight = "Cantidad")
    # Asignar el año correspondiente
    seg_year$year <- as.factor(year)
    seg_year$decomposition <- "Between province"
    # Agregar el resultado al dataframe creado anteriormente
    between_province <- bind_rows(between_province, seg_year)
  }
  between_province <- pivot_wider(between_province, names_from = stat, values_from = est)
  between_province
  
  # Descomposición del indice: "Segregacion dentro de provincias"
  within_province <- data.frame()
  for (year in 2011:2022) {
    # Filtrar por año y calcular la segregación total
    seg_year <- seg_migrantes %>% filter(anho == year) %>%
      mutual_total("Origen", "id", within = "provincia", weight = "Cantidad")
    # Asignar el año correspondiente
    seg_year$year <- as.factor(year)
    seg_year$decomposition <- "Within province"
    # Agregar el resultado al dataframe creado anteriormente
    within_province <- bind_rows(within_province, seg_year)
  }
  within_province <- pivot_wider(within_province, names_from = stat, values_from = est)
  within_province
  
  segregations <- bind_rows(segregations, between_province, within_province)
  
  # write_xlsx(segregations, "segregacion_total_prov.xlsx")
  
  rm(list = setdiff(ls(), c("r_a", "segregations", "seg_migrantes")))
  
  ### Volumen de migrantes y nativos por jurisdiccion. año 2022
  
vol_mig <- r_a %>% filter(anho == 2022) %>% 
  group_by(provincia) %>% 
  summarise(migrantes = sum(total_migrantes_primaria, na.rm = TRUE),
            nativos = sum(nativos, na.rm = TRUE)) %>% 
  arrange(desc(migrantes))
vol_mig$prop <- vol_mig$migrantes/sum(vol_mig$migrantes)

# write_xlsx(vol_mig, "migrantes_jurisdiccion.xlsx")
rm(vol_mig)  
  # mutual_total_nested(seg_migrantes, "Origen", c("provincia", "sector", "id"),
  #                     weight = "Cantidad")
  
  # Vemos segregación por diferentes variables
  
  by_sector <- data.frame()
  for (year in 2011:2022) {
    # Filtrar por año y calcular la segregación total
    seg_year <- seg_migrantes %>% filter(anho == year) %>%
      mutual_within("Origen", "id", within = "sector", weight = "Cantidad", wide = TRUE) %>% 
      arrange(desc(H))
    # Asignar el año correspondiente
    seg_year$year <- as.factor(year)
    # Agregar el resultado al dataframe creado anteriormente
    by_sector <- bind_rows(by_sector, seg_year)
  }
  by_sector
  
  by_ambito <- data.frame()
  for (year in 2011:2022) {
    # Filtrar por año y calcular la segregación total
    seg_year <- seg_migrantes %>% filter(anho == year) %>%
      mutual_within("Origen", "id", within = "ambito", weight = "Cantidad", wide = TRUE) %>% 
      arrange(desc(H))
    # Asignar el año correspondiente
    seg_year$year <- as.factor(year)
    # Agregar el resultado al dataframe creado anteriormente
    by_ambito <- bind_rows(by_ambito, seg_year)
  }
  by_ambito
  
  
  by_province <- data.frame()
  for (year in 2011:2022) {
    # Filtrar por año y calcular la segregación total
    seg_year <- seg_migrantes %>% filter(anho == year) %>%
      mutual_within("Origen", "id", within = "provincia", weight = "Cantidad", wide = TRUE) %>% 
      arrange(desc(H))
    # Asignar el año correspondiente
    seg_year$year <- as.factor(year)
    # Agregar el resultado al dataframe creado anteriormente
    by_province <- bind_rows(by_province, seg_year)
  }
  by_province
  
  
  by_origen <- data.frame()
  for (year in 2011:2022) {
    # Filtrar por año y calcular la segregación total
    seg_year <- seg_migrantes %>% filter(anho == year) %>%
      mutual_local("id", "Origen", weight = "Cantidad", wide = TRUE)
    # Asignar el año correspondiente
    seg_year$year <- as.factor(year)
    # Agregar el resultado al dataframe creado anteriormente
    by_origen <- bind_rows(by_origen, seg_year)
  }
  by_origen

# GRAFICOS

# grafico del indice de segregacion total
segregations %>% filter(decomposition == "Total") %>% 
  ggplot(aes(x = year, y = H, group = 1)) +
  geom_line(color = "blue") +
  labs(x = "Año", y = "Indice H", title = "Índice de segregación por años") +
  ylim(0,0.5)

ggplot(segregations, aes(x = year, y = H, color = decomposition, group = decomposition)) +
  geom_line() +
  labs(x = "Año", y = "Indice H", title = "Índice de segregación por años", color = "Descomposición") +
  ylim(0, 1)


ggplot(by_ambito, aes(x = year, y = H, color = ambito, group = ambito)) +
  geom_line() +
  labs(x = "Año", y = "Indice H", title = "Índice de segregación por años", color = "Ambito") +
  ylim(0, 1)

ggplot(by_sector, aes(x = year, y = H, color = sector, group = sector)) +
  geom_line() +
  labs(x = "Año", y = "Indice H", title = "Índice de segregación por años", color = "Sector") +
  ylim(0, 1)

ggplot(by_province, aes(x = year, y = H, color = provincia, group = provincia)) +
  geom_line(linewidth = 1) +
  scale_color_manual(values = rainbow(length(unique(by_province$provincia)))) +
  labs(x = "Año", y = "Indice H", title = "Índice de segregación por años", color = "Provincia")

by_origen %>% filter(Origen != "VenezuelaPrimaria") %>% 
ggplot(aes(x = year, y = ls, color = Origen, group = Origen)) +
  geom_line(linewidth = 1)  +
  labs(x = "Año", y = "Indice M", title = "Índice de segregación por años", color = "Origen")




